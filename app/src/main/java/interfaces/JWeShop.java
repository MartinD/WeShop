package interfaces;


import java.util.ArrayList;
import java.util.HashMap;

import vub.edu.weshop.model.Item;
import vub.edu.weshop.model.List;

/**
 * Created by martin on 11/04/2018.
 */

public interface JWeShop {
    public JWeShop registerATApp(ATWeShop weShop);
    public void authenticationFailed();
    public void authenticated();
    public void alone();
    public void presentLists(HashMap<String, List> lists);
    public List createForeignList(String username, String listName);
    public Item createForeignItem(String item, int quantity);
    public void addForeignItem(String item, int quantity, String listName);
    public void setItemQuantity(String itemName, String listName, int add);
    public int getIndex(ArrayList<Item> items, String itemName);
    public String getMyLists();
    public void mergeLists(String otherLists, HashMap<String, List> remoteLists);
    public ArrayList<List> getOwnLists();
    public void freezeList(String listName);

}
