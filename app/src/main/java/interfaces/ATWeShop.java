package interfaces;

import edu.vub.at.objects.coercion.Async;
import vub.edu.weshop.model.Item;
import vub.edu.weshop.model.List;

/**
 * Created by martin on 11/04/2018.
 */

public interface ATWeShop {

    void handshake(JWeShop gui);

    @Async
    void addItem(Item item, String listName);

    @Async
    void subscribeToList(List list);

    @Async
    void addList(List list, String listName);

    @Async
    void authentication(String value);

    @Async
    void searchList();

    @Async
    void itemQuantity(Item item, String listName, int add);

    @Async
    void mergeLists(List list);

    @Async
    void freezeList(String listName);

    void disconnect();
    void reconnect();

}
