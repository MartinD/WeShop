package vub.edu.weshop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import vub.edu.weshop.R;
import vub.edu.weshop.model.List;

/**
 * Created by martin on 31/05/18.
 */

public class ListAdapter extends ArrayAdapter<List> {


    public ListAdapter(@NonNull Context context, ArrayList<List> lists) {
        super(context, 0, lists);
    }

    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row, parent, false);
        }

        ListViewHolder listViewHolder = (ListViewHolder) convertView.getTag();

        if(listViewHolder == null){
            listViewHolder = new ListViewHolder();
            listViewHolder.listRow = (RelativeLayout) convertView.findViewById(R.id.listRow);
            listViewHolder.listName = (TextView) convertView.findViewById(R.id.list_name);
            listViewHolder.numberItems = (TextView) convertView.findViewById(R.id.number_items);
            convertView.setTag(listViewHolder);
        }

        List list = getItem(position);

        if(list.isFreezed()){
            listViewHolder.listRow.setBackgroundColor(Color.LTGRAY);
        } else {
            listViewHolder.listRow.setBackgroundColor(Color.TRANSPARENT);
        }
        listViewHolder.listName.setText(list.getName());
        listViewHolder.numberItems.setText(String.valueOf(list.getItems().size()));

        return convertView;
    }

    private class ListViewHolder{
        public RelativeLayout listRow;
        public TextView listName;
        public TextView numberItems;
    }
}
