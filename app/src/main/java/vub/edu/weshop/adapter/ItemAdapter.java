package vub.edu.weshop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Message;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import vub.edu.weshop.ItemListNameTuple;
import vub.edu.weshop.R;
import vub.edu.weshop.activity.MainActivity;
import vub.edu.weshop.model.Item;
import vub.edu.weshop.model.List;

/**
 * Created by martin on 2/06/18.
 */

public class ItemAdapter extends ArrayAdapter<Item> {

    private ArrayList<Item> items;
    private List list;

    public ItemAdapter(@NonNull Context context, ArrayList<Item> items) {
        super(context, 0, items);
        this.items = items;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_row, parent, false);
        }

        ItemViewHolder itemViewHolder = (ItemViewHolder) convertView.getTag();

        if(itemViewHolder == null){
            itemViewHolder = new ItemViewHolder();
            itemViewHolder.itemRow = (RelativeLayout) convertView.findViewById(R.id.itemRow);
            itemViewHolder.itemName = (TextView) convertView.findViewById(R.id.item_name);
            itemViewHolder.numberItem = (TextView) convertView.findViewById(R.id.number_item);
            itemViewHolder.minusButton = (Button) convertView.findViewById(R.id.minus_button);
            itemViewHolder.plusButton = (Button) convertView.findViewById(R.id.plus_button);
        }

        final Item item = getItem(position);

        if(list.isFreezed()){
            itemViewHolder.itemRow.setBackgroundColor(Color.LTGRAY);
        }
        itemViewHolder.itemName.setText(item.getItem());
        itemViewHolder.numberItem.setText(String.valueOf(item.getQuantity()));
        final ItemViewHolder finalItemViewHolder = itemViewHolder;
        itemViewHolder.plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!list.isFreezed()){
                    item.increment();
                    finalItemViewHolder.numberItem.setText(String.valueOf(item.getQuantity()));
                    MainActivity.mHandler.sendMessage(Message.obtain(MainActivity.mHandler, MainActivity.MSG_ITEM_QUANTITY_PLUS, new ItemListNameTuple(item, list.getName())));
                }
            }
        });
        itemViewHolder.minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!list.isFreezed()){
                    item.decrement();
                    finalItemViewHolder.numberItem.setText(String.valueOf(item.getQuantity()));
                    MainActivity.mHandler.sendMessage(Message.obtain(MainActivity.mHandler, MainActivity.MSG_ITEM_QUANTITY_MINUS, new ItemListNameTuple(item, list.getName())));
                }
            }
        });

        return convertView;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public void setList(List list){
        this.list = list;
    }

    private class ItemViewHolder{

        public RelativeLayout itemRow;
        public TextView itemName;
        public TextView numberItem;
        public Button minusButton;
        public Button plusButton;
    }
}
