package vub.edu.weshop.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import interfaces.JWeShop;
import vub.edu.weshop.ItemListNameTuple;
import vub.edu.weshop.R;
import vub.edu.weshop.adapter.ItemAdapter;
import vub.edu.weshop.model.Item;
import vub.edu.weshop.model.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemsFragment extends Fragment{

    private List list;
    public static Handler itemViewHandler;
    private ItemAdapter itemAdapter;
    private Context context;
    private String UUID;
    private int index;

    public ItemsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        ItemViewThread itemViewThread = new ItemViewThread();
        itemViewThread.start();
        itemViewHandler = itemViewThread.viewHandler;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_items, container, false);
        this.context = getContext();
        ListView listItems = view.findViewById(R.id.listItems);
        itemAdapter = new ItemAdapter(context, list.getItems());
        itemAdapter.setList(list
        );
        listItems.setAdapter(itemAdapter);
        getActivity().setTitle("Items from " + list.getName());
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.list_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.addItem:
                addItem();
                return true;
            case R.id.freeze:
                freezeList();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addItem(){
        if(!list.isFreezed()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View addItem = getLayoutInflater().inflate(R.layout.dialog_item, null);
            final EditText newItem = addItem.findViewById(R.id.newItem);
            builder.setTitle("Choose a name for the item:");
            builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Item item = new Item(newItem.getText().toString(), 1);
                    if (!list.getItems().contains(item)) {                            // If item doesn't exist then add it to the list otherwise increment the counter
                        list.getItems().add(item);
                        ItemListNameTuple itemListNameTuple = new ItemListNameTuple(item, list.getName());
                        MainActivity.mHandler.sendMessage(Message.obtain(MainActivity.mHandler, MainActivity.MSG_ADD_ITEM, itemListNameTuple));
                    } else {
                        list.getItems().get(list.getItems().indexOf(item)).increment();
                        ItemListNameTuple itemListNameTuple = new ItemListNameTuple(item, list.getName());
                        MainActivity.mHandler.sendMessage(Message.obtain(MainActivity.mHandler, MainActivity.MSG_ITEM_QUANTITY_PLUS, itemListNameTuple));
                    }
                    itemAdapter.notifyDataSetChanged();

                }
            });
            builder.setView(addItem);
            builder.setCancelable(true);
            AlertDialog itemDialog = builder.create();
            itemDialog.show();
        }
    }

    private void freezeList(){
        if(!list.isFreezed()){
            list.setFreezed(true);
            MainActivity.mHandler.sendMessage(Message.obtain(MainActivity.mHandler, MainActivity.MSG_FREEZE_LIST, list.getName()));
        }
    }

    public void setList(List list){
        this.list = list;
    }

    public void setUUID(String UUID){
        this.UUID = UUID;
    }

    public void setIndex(int index){
        this.index = index;
    }

    public static final int TEST = 11;
    public static final int UPDATE_ITEM_NUMBER = 12;
    public static final int UPDATE_ITEMS = 13;
    public static final int LIST_ADDED = 14;

    public class ItemViewThread extends Thread {
        @SuppressLint("HandlerLeak")
        public Handler viewHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (null == MainActivity.atWS)
                    return;
                switch (msg.what) {
                    case TEST:
                        String[] values = (String[]) msg.obj;
                        Toast.makeText(context, values[0] + " from " + values[1], Toast.LENGTH_LONG).show();
                        break;
                    case UPDATE_ITEM_NUMBER:
                        String[] names = (String[]) msg.obj;
                        itemAdapter.notifyDataSetChanged();
                        Toast.makeText(context, "Quantity changed for " + names[0] + " in " + names[1], Toast.LENGTH_SHORT).show();
                        break;
                    case UPDATE_ITEMS:
                        itemAdapter.notifyDataSetChanged();
                        break;

                }
            }
        };

        public void run() {
            Looper.prepare();
            Looper.loop();
        }
    }
}
