package vub.edu.weshop.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.util.PrintStreamPrinter;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

import edu.vub.at.IAT;
import edu.vub.at.android.util.IATAndroid;
import interfaces.ATWeShop;
import interfaces.JWeShop;
import vub.edu.weshop.ItemListNameTuple;
import vub.edu.weshop.R;
import vub.edu.weshop.WeShopAssetInstaller;
import vub.edu.weshop.model.Item;
import vub.edu.weshop.model.List;

public class MainActivity extends AppCompatActivity implements JWeShop {

    class StartIATTask extends AsyncTask<Void,String,Void> {

        private ProgressDialog pd;

        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            pd.setMessage(values[0]);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(MainActivity.this, "WeShop", "Starting AmbientTalk");
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pd.dismiss();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                iat = IATAndroid.create(MainActivity.this);
                this.publishProgress("Loading WeShop code");
                iat.evalAndPrint("import /.WeShop.WeShop.makeWeShop()", System.err);
            } catch (Exception e) {
                Log.e("AmbientTalk","Could not start IAT",e);
            }
            return null;
        }

    }

    public static IAT iat;
    public static ATWeShop atWS;

    private ArrayList<List> lists = new ArrayList<>();
    private ArrayList<List> otherLists = new ArrayList<>();
    private String UUID = "ME";
    private AlertDialog identificationDialog;

    public static Handler mHandler;

    private ListsFragment listsFragment = new ListsFragment();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.verifyStoragePermissions(this);
        //weShopView = new WeShopView(R.);
        setContentView(R.layout.activity_main);
        if (iat == null) {
            Intent i = new Intent(this,WeShopAssetInstaller.class);
            startActivityForResult(i,0);
        }

        listsFragment.setUUID(UUID);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, listsFragment);
        fragmentTransaction.commit();


        userIdentification();



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == 0){
            if (resultCode == Activity.RESULT_OK) {
                Log.v("WeShop","Starting Asset Installer");

                LooperThread lt = new LooperThread();
                lt.start();
                mHandler = lt.mHandler;

                new StartIATTask().execute((Void)null);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(atWS != null){
            atWS.handshake(this);
        }
    }

    private void authentication(String value){
        mHandler.sendMessage(Message.obtain(mHandler, MainActivity.MSG_NEW_USER, value));
    }

    void subscribeToList(List list){
        mHandler.sendMessage(Message.obtain(mHandler, MainActivity.MSG_SUBSCRIBE_TO_LIST, list));
    }

    public void authenticationFailed(){
        ListsFragment.listViewHandler.sendMessage(Message.obtain(ListsFragment.listViewHandler, ListsFragment.AUTHENTICATION_FAILED, null));
    }

    public void authenticated(){
        ListsFragment.listViewHandler.sendMessage(Message.obtain(ListsFragment.listViewHandler, ListsFragment.AUTHENTICATION_SUCCEED, null));
    }

    public void alone(){
        ListsFragment.listViewHandler.sendMessage(Message.obtain(ListsFragment.listViewHandler, ListsFragment.ALONE, null));
    }

    @Override
    public void presentLists(HashMap<String, List> allLists) {
        otherLists.clear();
        otherLists.addAll(allLists.values());
    }

    @Override
    public List createForeignList(String username, String listName){
        return new List(listName, new ArrayList<Item>(), false);
    }

    @Override
    public Item createForeignItem(String itemName, int quantity){
        return new Item(itemName, quantity);
    }

    @Override
    public void addForeignItem(String itemName, int quantity, String listName){
        Item item = new Item(itemName, quantity);
        for(List list : lists){
            if(list.getName().equals(listName)){
                if(!list.getItems().contains(item))
                    list.getItems().add(item);
                else{
                    Item currentItem = list.getItems().get(list.getItems().indexOf(item));
                    currentItem.setQuantity(currentItem.getQuantity() + item.getQuantity());
                }
            }
        }
        ListsFragment.listViewHandler.sendMessage(Message.obtain(ListsFragment.listViewHandler, ListsFragment.UPDATE_NUMBER_ITEMS, listName));
        ItemsFragment.itemViewHandler.sendMessage(Message.obtain(ItemsFragment.itemViewHandler, ItemsFragment.UPDATE_ITEMS, null));
    }

    @Override
    public void setItemQuantity(String itemName, String listName, int add){
        for(List list : lists){
            if(list.getName().equals(listName)){
                for(Item item : list.getItems()){
                    if(item.getItem().equals(itemName)){
                        item.setQuantity(item.getQuantity() + add);
                    }
                }
            }
        }
        String[] names = new String[]{itemName, listName};
        ItemsFragment.itemViewHandler.sendMessage(Message.obtain(ItemsFragment.itemViewHandler, ItemsFragment.UPDATE_ITEM_NUMBER, names));
    }

    public int getIndex(ArrayList<Item> items, String itemName){
        for(int i=0 ; i<items.size(); i++){
            Item item = items.get(i);
            if(item.getItem().equals(itemName)){
                return i;
            }
        }
        return 0;
    }

    @Override
    public String getMyLists(){
        Gson gson = new Gson();
        return gson.toJson(lists);
    }

    @Override
    public void mergeLists(String JSONOtherLists, HashMap<String, List> remoteLists){
        Gson gson = new Gson();
        ArrayList<List> otherLists = gson.fromJson(JSONOtherLists, new TypeToken<ArrayList<List>>(){}.getType());
        ArrayList<List> remoteArrayLists = new ArrayList<>();
        remoteArrayLists.addAll(remoteLists.values());
        for(List list : otherLists){
            if(!remoteArrayLists.contains(list)){
                atWS.mergeLists(list);
            }
        }
    }

    public void freezeList(String listName){
        for(List list : lists){
            if(list.getName().equals(listName)){
                list.setFreezed(true);
            }
        }
    }

    public ArrayList<List> getOwnLists(){
        return lists;
    }

    private void userIdentification(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View userIdentification = getLayoutInflater().inflate(R.layout.dialog_username, null);
        final EditText username = userIdentification.findViewById(R.id.username);
        builder.setTitle("Choose a username:");
        builder.setPositiveButton("Let's go!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setView(userIdentification);
        builder.setCancelable(false);
        identificationDialog = builder.create();
        identificationDialog.show();     //must be before getButton
        identificationDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UUID = username.getText().toString();
                authentication(UUID);
            }
        });
    }

    public JWeShop registerATApp(ATWeShop atWS){
        this.atWS = atWS;
        return this;
    }

    public ArrayList<List> getLists(){
        return lists;
    }

    public void setLists(ArrayList<List> lists){
        this.lists = lists;
    }

    public ArrayList<List> getOtherLists(){
        return otherLists;
    }

    public void setOtherLists(ArrayList<List> otherLists){
        this.otherLists = otherLists;
    }

    public String getUUID(){
        return UUID;
    }

    public void setUUID(String UUID){
        this.UUID = UUID;
    }

    public AlertDialog getIdentificationDialog(){
        return identificationDialog;
    }


    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    public static final int MSG_NEW_USER = 0;
    public static final int MSG_SEARCH_LIST = 1;
    public static final int MSG_ADD_ITEM = 2;
    public static final int MSG_ADD_LIST = 3;
    public static final int MSG_SUBSCRIBE_TO_LIST = 4;
    public static final int MSG_ITEM_QUANTITY_PLUS = 5;
    public static final int MSG_ITEM_QUANTITY_MINUS = 6;
    public static final int MSG_FREEZE_LIST = 7;
    public static final int MSG_TEST = 10;


    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    // Call the AmbientTalk methods in a separate thread to avoid blocking the UI.
    class LooperThread extends Thread {

        @SuppressLint("HandlerLeak")
        public Handler mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (null == atWS)
                    return;
                switch (msg.what) {
                    case MSG_NEW_USER: {
                        String value = (String) msg.obj;
                        atWS.authentication(value);
                        break;
                    }
                    case MSG_ADD_LIST: {
                        List list = (List) msg.obj;
                        atWS.addList(list, list.getName());
                        break;
                    }
                    case MSG_SEARCH_LIST:{
                        atWS.searchList();
                        break;
                    }
                    case MSG_SUBSCRIBE_TO_LIST:{
                        List list = (List) msg.obj;
                        atWS.subscribeToList(list);
                        break;
                    }
                    case MSG_ADD_ITEM: {
                        ItemListNameTuple itemListNameTuple = (ItemListNameTuple) msg.obj;
                        atWS.addItem(itemListNameTuple.getItem(), itemListNameTuple.getListName());
                        break;
                    }
                    case MSG_ITEM_QUANTITY_PLUS: {
                        ItemListNameTuple itemListNameTuple = (ItemListNameTuple) msg.obj;
                        atWS.itemQuantity(itemListNameTuple.getItem(), itemListNameTuple.getListName(), 1);
                        break;
                    }
                    case MSG_ITEM_QUANTITY_MINUS: {
                        ItemListNameTuple itemListNameTuple = (ItemListNameTuple) msg.obj;
                        atWS.itemQuantity(itemListNameTuple.getItem(), itemListNameTuple.getListName(), -1);
                        break;
                    }
                    case MSG_FREEZE_LIST: {
                        String listName = (String) msg.obj;
                        atWS.freezeList(listName);
                        break;
                    }
                }
            }
        };

        public void run() {
            Looper.prepare();
            Looper.loop();
        }
    }


}
