package vub.edu.weshop.activity;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import vub.edu.weshop.R;
import vub.edu.weshop.adapter.ListAdapter;
import vub.edu.weshop.model.Item;
import vub.edu.weshop.model.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListsFragment extends Fragment {


    public static Handler listViewHandler;
    private ItemsFragment itemsFragment = new ItemsFragment();
    private Context context;
    private String UUID = "";
    private ListAdapter adapter;
    private ListAdapter otherAdapter;
    private boolean activate = true;

    public ListsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        ViewThread vt = new ViewThread();
        vt.start();
        listViewHandler = vt.viewHandler;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lists, container, false);

        this.context = getContext();
        adapter = new ListAdapter(context, ((MainActivity) getActivity()).getLists());
        otherAdapter = new ListAdapter(context, ((MainActivity) getActivity()).getOtherLists());
        ListView listLists = view.findViewById(R.id.listLists);
        listLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                List list = ((MainActivity) getActivity()).getLists().get(i);
                itemsFragment.setIndex(i);
                itemsFragment.setList(list);
                itemsFragment.setUUID(UUID);
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, itemsFragment).addToBackStack( "tag" );
                fragmentTransaction.commit();
            }
        });
        listLists.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("WeShop : " + UUID);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_item:
                addList();
                return true;
            case R.id.search_item:
                searchList();
                return true;
            case R.id.connection:
                if (activate) {
                    MainActivity.atWS.disconnect();
                    activate = false;
                } else{
                    MainActivity.atWS.reconnect();
                    activate = true;
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addList() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View addList = getLayoutInflater().inflate(R.layout.dialog_list, null);
        final EditText newList = addList.findViewById(R.id.newList);
        builder.setTitle("Choose a name for the list:");
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setView(addList);
        builder.setCancelable(true);
        final AlertDialog listDialog = builder.create();
        listDialog.show();
        listDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List list = new List(newList.getText().toString(), new ArrayList<Item>(), false);
                if(!((MainActivity) getActivity()).getLists().contains(list) && !((MainActivity) getActivity()).getOtherLists().contains(list)){
                    ((MainActivity) getActivity()).getLists().add(list);
                    adapter.notifyDataSetChanged();
                    MainActivity.mHandler.sendMessage(Message.obtain(MainActivity.mHandler, MainActivity.MSG_ADD_LIST, list));
                } else {
                    listViewHandler.sendMessage(Message.obtain(listViewHandler, EXISTS_ALREADY, null));
                }
                listDialog.cancel();
            }
        });
    }

    private void searchList(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View searchList = getLayoutInflater().inflate(R.layout.dialog_other_lists, null);
        builder.setTitle("Lists of other peers:");
        ListView otherListLists = (ListView) searchList.findViewById(R.id.otherLists);
        otherListLists.setAdapter(otherAdapter);
        builder.setView(searchList);
        builder.setCancelable(true);
        final AlertDialog searchListDialog = builder.create();
        searchListDialog.show();
        otherListLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ((MainActivity) getActivity()).getLists().add(((MainActivity) getActivity()).getOtherLists().get(i));
                adapter.notifyDataSetChanged();
                ((MainActivity) getActivity()).subscribeToList(((MainActivity) getActivity()).getOtherLists().get(i));
                ((MainActivity) getActivity()).getOtherLists().remove(((MainActivity) getActivity()).getOtherLists().get(i));
                searchListDialog.cancel();
            }
        });
        MainActivity.mHandler.sendMessage(Message.obtain(MainActivity.mHandler, MainActivity.MSG_SEARCH_LIST, null));
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public static final int AUTHENTICATION_FAILED = 12;
    public static final int AUTHENTICATION_SUCCEED = 13;
    public static final int ALONE = 14;
    public static final int UPDATE_NUMBER_ITEMS = 15;
    public static final int EXISTS_ALREADY = 16;

    class ViewThread extends Thread {
        @SuppressLint("HandlerLeak")
        public Handler viewHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (null == MainActivity.atWS)
                    return;
                switch (msg.what) {
                    case AUTHENTICATION_SUCCEED:
                        Toast.makeText(context, "Welcome to WeShop !", Toast.LENGTH_LONG).show();
                        ((MainActivity) getActivity()).getIdentificationDialog().cancel();
                        UUID = ((MainActivity) getActivity()).getUUID();
                        getActivity().setTitle("WeShop : " + UUID);
                        break;
                    case AUTHENTICATION_FAILED:
                        Toast.makeText(context, "Username already taken", Toast.LENGTH_LONG).show();
                        break;
                    case UPDATE_NUMBER_ITEMS:
                        String listName = (String) msg.obj;
                        adapter.notifyDataSetChanged();
                        Toast.makeText(context, "An item has been added in " + listName, Toast.LENGTH_SHORT).show();
                        //otherAdapter.notifyDataSetChanged();
                        break;
                    case ALONE:
                        Toast.makeText(context, "You are alone on the network", Toast.LENGTH_LONG).show();
                        ((MainActivity) getActivity()).getIdentificationDialog().cancel();
                        UUID = ((MainActivity) getActivity()).getUUID();
                        getActivity().setTitle("WeShop : " + UUID);
                        break;
                    case EXISTS_ALREADY:
                        Toast.makeText(context, "The list has already been created, either by you or by an other peer", Toast.LENGTH_LONG).show();
                        break;

                }
            }
        };

        public void run() {
            Looper.prepare();
            Looper.loop();
        }
    }
}
