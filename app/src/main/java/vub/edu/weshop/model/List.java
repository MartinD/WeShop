package vub.edu.weshop.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by martin on 31/05/18.
 */

public class List {

    private String name;
    private ArrayList<Item> items;
    private boolean freezed = false;

    public List(String name, ArrayList<Item> items, boolean freezed){
        this.name = name;
        this.items = items;
        this.freezed = freezed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public boolean isFreezed() {
        return freezed;
    }

    public void setFreezed(boolean freezed) {
        this.freezed = freezed;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        if(obj != null && obj instanceof List){
            isEqual = (this.getName().equals(((List) obj).getName()));
        }
        return isEqual;
    }
}
