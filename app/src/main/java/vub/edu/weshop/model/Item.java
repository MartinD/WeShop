package vub.edu.weshop.model;

import android.util.Log;

/**
 * Created by martin on 31/05/18.
 */

public class Item {

    private String item;
    private int quantity;

    public Item(String item, int quantity){
        this.item = item;
        this.quantity = quantity;
    }

    public void increment(){
        quantity += 1;
    }

    public void decrement(){
        if(quantity > 0)
            quantity -= 1;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        if(obj != null && obj instanceof Item){
            isEqual = (this.getItem().equals( ((Item) obj).getItem()));
        }
        Log.e("WeShop", "ITEMS EQUAL : " + isEqual);
        return isEqual;
    }
}
