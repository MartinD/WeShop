package vub.edu.weshop;

import vub.edu.weshop.model.Item;

/**
 * Created by martin on 4/06/18.
 */

public class ItemListNameTuple {

    private Item item;
    private String listName;

    public ItemListNameTuple(Item item, String listName){
        this.item = item;
        this.listName = listName;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }



}
