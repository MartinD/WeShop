# WeShop

WeShop is a ditributed mobile application realised for the course "Distributed and Mobile Programming Paradigms INFO-Y-082" at the VUB.

## Installation

In order to install the application and test it, either send me a mail to mrt.dlb@gmail.com so that I can add you to the pool of testers from Fabric.io or just clone the repository.
Keep in mind that this project has been developed within the Android Studio IDE and is only a development version.

## Usability

The application has been tested on Android 4.1.1 and on Android 7.0. The application worked on both of these platforms but Android 4.1.1 was only tested with emulators while Android 7.0 was only tested with real devices (Huawei P9 and Huawei honor 5). It is required to use a local wifi and to be connected to the wifi in order to use all the features of the application.

## Informations

The distributed part of the application has been realised with AmbientTalk, a language developed by the VUB <http://soft.vub.ac.be/amop/at/tutorial/tutorial>.

If you want more informations about the application and its architecture, read the DMPP_Project.pdf file included in this git.
